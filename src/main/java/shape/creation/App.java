/*
 * Copyright 2019 Thales Italia spa.
 * 
 * This program is not yet licensed and this file may not be used under any
 * circumstance.
 */
package shape.creation;

/**
 * Hello world!
 *
 */
public class App {
  public static void main(String[] args) {

    final ShapeMacker shapeMacker = new ShapeMacker(new Circle(), new Rectangle(), new Square());
    shapeMacker.drawCircle();
    shapeMacker.drawRectangle();
    shapeMacker.drawSquare();
  }
}
