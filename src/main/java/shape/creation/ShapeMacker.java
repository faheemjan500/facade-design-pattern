/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package shape.creation;

import shapes.Shape;

public class ShapeMacker {

  private final Shape circle;
  private final Shape rectangle;
  private final Shape square;

  public ShapeMacker(Shape circle, Shape rectangle, Shape square) {
    this.circle = circle;
    this.rectangle = rectangle;
    this.square = square;
  }

  public void drawCircle() {
    circle.draw();
  }

  public void drawRectangle() {
    rectangle.draw();
  }

  public void drawSquare() {
    square.draw();
  }

}
