/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package shape.creation;

import shapes.Shape;

public class Circle implements Shape {

  public void draw() {

    System.out.println("Circle is drawn! \n");
  }

}
